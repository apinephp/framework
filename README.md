APIne Framework
================

APIne is the most flexible MVC Framework. It is small yet it has all you need to build a powerful PHP application.

The APIne environment has a query builder, a comprehensive session manager, a basic yet effective ORM, and more ready to be installed as optional packages that fully integrate with the Framework.

## Requirements

* PHP 7.2 or greater
* Apache 2.4 with mod_rewrite

The project using APIne must be installed at the root of a host that must include the instruction `AllowOverride FileInfo Options Indexes` for the default settings. PHP's user must also have writing permissions on the project directory.

> APIne does not officially support any other HTTP server (nginx, lighttpd, ...). If you are using one of those you might need modify your server's configuration.

## Installation

Installation is made with Composer.

```sh
composer require apinephp/framework
```

The project is currently under heavy development thus is not ready for use. It is recommended to install the stable version of the framework instead.

```sh
composer require apinephp/legacy-framework
```

## Learn More

Learn More at the following links :

- [Documentation](https://gitlab.com/apinephp/framework/wikis/home)
- [Example Project](https://github.com/Youmy001/apine-framework-example)

## Support

- [Report an issue](https://gitlab.com/apinephp/framework/issues)
- [Public Wiki](https://gitlab.com/apinephp/framework/wikis/home)

## License

The APIne Framework is distributed under the MIT license. See the [LICENSE file](https://gitlab.com/apinephp/framework/blob/master/LICENSE) for more information.
