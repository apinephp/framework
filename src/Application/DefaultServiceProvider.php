<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Application;

use Apine\Application\DependencyResolver;
use Apine\Core\Config\Config;
use Apine\Core\Config\Handlers\JsonFileHandler;
use Apine\Core\Views\FileView;
use Apine\Core\Views\HTMLView;
use Apine\Core\Views\JSONView;
use Apine\Core\Views\PHPView;
use Apine\Core\Views\RedirectionView;
use Apine\Core\Views\Twig\ExtensionLoader;
use Apine\Core\Views\URLHelper;
use Apine\Core\Views\XMLView;
use Apine\DistRoute\Router;
use Apine\DistRoute\RouterInterface;
use Apine\Http\Factories\RequestFactory;
use Apine\Http\Factories\ResponseFactory;
use Apine\Http\Factories\UriFactory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use ReflectionClass;
use ReflectionMethod;
use function function_exists;
use function in_array, array_map, array_filter;
use function substr, strtoupper, ucwords, strtolower, str_replace;
use function json_decode, file_get_contents;

/**
 * Class DefaultServiceProvider
 *
 * @package Apine\Application
 */
final class DefaultServiceProvider
{
    public function register(ServiceContainer $container): void
    {
        if (!$container->has('request')) {
            $container->register('request', function (): ServerRequestInterface {
                
                if (!function_exists('getallheaders')) {
                    function getallheaders()
                    {
                        $headers = [];
                        foreach ($_SERVER as $name => $value) {
                            if (strpos($name, 'HTTP_') === 0) {
                                $headers[str_replace(' ', '-',
                                    ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                            }
                        }
                        
                        return $headers;
                    }
                }
                
                return (new RequestFactory())->createServerRequestFromGlobals(
                    $_SERVER,
                    $_GET,
                    $_POST,
                    $_FILES,
                    $_COOKIE,
                    getallheaders(),
                    file_get_contents('php://input')
                );
            });
        }
        
        if (!$container->has('response')) {
            $container->register('response', function (): ResponseInterface {
                return (new ResponseFactory())->createResponse();
            }, true);
        }
        
        if (!$container->has('uri')) {
            $container->register('uri', function (): UriInterface {
                return (new UriFactory())->createUriFromArray($_SERVER);
            });
        }
        
        if (!$container->has('router')) {
            $container->register('router', function (ContainerInterface $container): RouterInterface {
                $router = new Router($container);
                
                $config = new Config(new JsonFileHandler('config/router.json'));
                
                try {
                    if ($config->serve->api === true) {
                        $router->group($config->prefixes->api, function (RouterInterface $mapper) {
                            $routes = json_decode(file_get_contents('config/routes/api.json'), true);
                            
                            foreach ($routes as $pattern => $definitions) {
                                $controller = $definitions['controller'];
                                $reflection = new ReflectionClass($definitions['controller']);
                                unset($definitions['controller']);
                                
                                array_map(function (ReflectionMethod $method) use ($pattern, $controller, $mapper) {
                                    $requestMethod = strtoupper($method->getName());
                                    
                                    $mapper->map(
                                        [$requestMethod],
                                        $pattern,
                                        $controller . '@' . $method->getName()
                                    );
                                }, array_filter(
                                    $reflection->getMethods(ReflectionMethod::IS_PUBLIC),
                                    function (ReflectionMethod $method) {
                                        return in_array(strtoupper($method->getName()), Router::$verbs, true);
                                    }
                                ));
                            }
                        });
                    }
                    
                    if ($config->serve->web === true) {
                        $router->group($config->prefixes->web, function (RouterInterface $mapper) {
                            $routes = json_decode(file_get_contents('config/routes/web.json'), true);
                            
                            foreach ($routes as $pattern => $definitions) {
                                foreach ($definitions as $method => $definition) {
                                    $mapper->map(
                                        [$method],
                                        $pattern,
                                        $definition['controller'] . '@' . $definition['action']
                                    );
                                }
                            }
                        });
                    }
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage(), $e->getCode(), $e);
                }
                
                return $router;
            });
        }
        
        if (!$container->has('twigEnvironment')) {
            $container->register('twigEnvironment', function (ContainerInterface $container): \Twig_Environment {
                $settings = $container->get('settings');
                
                $loader = new \Twig_Loader_Filesystem($settings->includePath);
                $twig = new \Twig_Environment($loader, array(
                    'cache'       => 'views/_cache',
                    'auto_reload' => true,
                    'debug'       => $settings->error['reportingLevel']
                ));
    
                $resolver = new DependencyResolver($container);
                $loader = new ExtensionLoader($twig, $resolver);
                
                foreach ($settings->twig['extensions'] as $extension) {
                    $twig = $loader->addExtension($extension);
                }
                
                return $twig;
            }, true);
        }
        
        if (!$container->has('HTMLView')) {
            $container->register('HTMLView', function (ContainerInterface $container): HTMLView {
                $twig = $container->get('twigEnvironment');
                $response = $container->get('response');
                return new HTMLView($response, $twig);
            });
        }
    
        if (!$container->has('fileView')) {
            $container->register('fileView', function (ContainerInterface $container): FileView {
                $response = $container->get('response');
                return new FileView($response);
            });
        }
    
        if (!$container->has('JSONView')) {
            $container->register('JSONView', function (ContainerInterface $container): JSONView {
                $response = $container->get('response');
                return new JSONView($response);
            });
        }
    
        if (!$container->has('PHPView')) {
            $container->register('PHPView', function (ContainerInterface $container): PHPView {
                $response = $container->get('response');
                return new PHPView($response);
            });
        }
    
        if (!$container->has('RedirectionView')) {
            $container->register('redirectionView', function (ContainerInterface $container): RedirectionView {
                $response = $container->get('response');
                $factory = new UriFactory();
                return new RedirectionView($response, $factory);
            });
        }
    
        if (!$container->has('XMLView')) {
            $container->register('XMLView', function (ContainerInterface $container): XMLView {
                $response = $container->get('response');
                return new XMLView($response);
            });
        }
        
        if (!$container->has('urlHelper')) {
            $container->register('urlHelper', function (ContainerInterface $container): URLHelper {
                return new URLHelper($container->get('uri'));
            });
        }
    }
}