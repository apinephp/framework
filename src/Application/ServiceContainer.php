<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Application;

use Apine\Container\Container;
use Apine\Core\Config\Config;
use Apine\Core\Config\Handlers\ArrayHandler;
use Apine\Core\Views\Twig\Extensions\CookieExtension;
use Apine\Core\Views\Twig\Extensions\ExecutionTimeExtension;
use Apine\Core\Views\Twig\Extensions\UriExtension;
use Closure;

/**
 * Class ServiceContainer
 *
 * @package Apine\Application
 */
class ServiceContainer extends Container
{
    static private $defaultSettings = [
        'httpVersion' => '1.1',
        'error' => [
            'showTrace' => true,
            'reportingLevel'=> E_USER_ERROR | E_USER_WARNING
        ],
        'twig' => [
            'extensions' => [
                CookieExtension::class,
                UriExtension::class,
                ExecutionTimeExtension::class,
            ]
        ]
    ];
    
    /**
     * ServiceContainer constructor.
     *
     * @param array $services
     */
    public function __construct(array $services = [])
    {
        $userSettings = $services['settings'] ?? [];
        $this->registerDefaultServices($services, $userSettings);
    }
    
    private function registerDefaultServices(array $services, array $userSettings): void
    {
        $defaultSettings = self::$defaultSettings;
        
        foreach ($services as $serviceName => $serviceValue) {
            if ($serviceValue instanceof Closure) {
                $this->register($serviceName, $serviceValue);
            } else {
                $this->register($serviceName, function () use ($serviceValue) {
                    return $serviceValue;
                });
            }
        }
        
        $this->register('settings', function () use ($defaultSettings, $userSettings) {
            return new Config(new ArrayHandler(array_merge($defaultSettings, $userSettings)));
        });
        
        $provider = new DefaultServiceProvider();
        $provider->register($this);
    }
    
    public function register(string $id, Closure $function, bool $factory = false): void
    {
        parent::register($id, $function, $factory);
    }
}