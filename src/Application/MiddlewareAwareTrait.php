<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Application;

use Closure;
use Apine\Dispatcher\MiddlewareQueue;
use Apine\Dispatcher\MiddlewareQueueException;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;

/**
 * Trait MiddlewareAwareTrait
 *
 * @package Apine\Application
 */
trait MiddlewareAwareTrait
{
    /**
     * @var MiddlewareQueue
     */
    protected $queue;
    
    /**
     * @param MiddlewareInterface | Closure $middleware
     *
     * @throws MiddlewareQueueException
     */
    public function addMiddleware($middleware) : void
    {
        if (null === $this->queue) {
            $this->queue = new MiddlewareQueue();
        }
        
        if ($middleware instanceof Closure) {
            if (
                property_exists($this, 'container') &&
                $this->container instanceof ContainerInterface
            ) {
                $middleware = $middleware($this->container);
            } else {
                $middleware = $middleware();
            }
        }
    
        $this->queue->add($middleware);
    }
}