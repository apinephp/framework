<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Application;

use Apine\Core\Error\ErrorHandler;
use Apine\Core\Error\Http\HttpException;
use Apine\Dispatcher\Dispatcher;
use Apine\Http\Response;
use Apine\Http\Stream;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use function dirname, is_array, sprintf, fopen, explode, implode;
use function headers_sent, header;
use function Apine\Core\Utility\executionTime;

/**
 * Apine Application
 *
 * This is the main application class. This is the class
 * user instantiate and link on services and middlewares.
 * This is the class that manages the execution of the
 * application.
 *
 * @author Tommy Teasdale <tteasdaleroads@gmail.com>
 * @package Apine\Application
 */
final class Application
{
    use ContainerAwareTrait;
    use MiddlewareAwareTrait;
    
    /**
     * Name of the folder where the framework is located
     *
     * @var string
     */
    private $apineFolder;
    
    /**
     * @var string
     */
    private $includePath;
    
    /**
     * Application constructor.
     *
     * @param ContainerInterface|array $container
     * @param string|null        $projectDirectory
     */
    public function __construct($container = [], string $projectDirectory = null)
    {
        try {
            executionTime();
            $this->setPath($projectDirectory);
            
            ErrorHandler::set(E_ALL, true);
            
            if (is_array($container)) {
                $container = new ServiceContainer($container);
            }
    
            if (!$container instanceof ContainerInterface) {
                throw new InvalidArgumentException('Expected a ContainerInterface');
            }
            
            $this->container = $container;
    
            if ($this->container->get('settings')) {
                $settings = $this->container->get('settings');
                ErrorHandler::set(
                    $settings->error['reportingLevel'],
                    $settings->error['showTrace']
                );
                $settings->includePath = $this->includePath;
            }
            
        } catch (\Exception $e) {
            $this->outputException($e);
            die();
        }
        
    }
    
    /**
     * Move the include path to the project's root
     * rather than the server's root
     *
     * @param string|null $projectDirectory
     */
    private function setPath(string $projectDirectory = null) : void
    {
        $documentRoot = $_SERVER['DOCUMENT_ROOT'];
        $this->apineFolder = dirname(__FILE__, 2) . '/'; // The path to the framework itself
        
        if (null === $projectDirectory) {
            $directory = $documentRoot;
        
            while (!file_exists($directory . '/composer.json')) {
                $directory = dirname($directory);
            }
        
            $projectDirectory = $directory;
        }
    
        $this->includePath = $projectDirectory;
        set_include_path($this->includePath);
        chdir($this->includePath);
    }
    
    /**
     * Run the application
     */
    public function run() : void
    {
        $config = null;
        
        /**
         * Main Execution
         */
        try {
            $request = $this->container->get('request');
            $router = $this->container->get('router');
            
            $dispatcher = new Dispatcher($router);
            
            if ($this->queue !== null) {
                $dispatcher = $dispatcher->withMiddlewareQueue($this->queue->getQueue());
            }
            
            $response = $dispatcher->handle($request);
            
            $this->output($response);
        } catch (\Throwable $e) {
            $this->outputException($e);
        }
    }
    
    /**
     * Output a response to the client
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     */
    public function output(ResponseInterface $response) : void
    {
        if (!headers_sent()) {
            header(sprintf(
                'HTTP/%s %s %s',
                $response->getProtocolVersion(),
                $response->getStatusCode(),
                $response->getReasonPhrase()
            ));
        
            foreach ($response->getHeaders() as $name => $values) {
                if (is_array($values)) {
                    $values = implode(', ', $values);
                }
            
                header(sprintf('%s: %s', $name, $values), false);
            }
        }
        
        $body = $response->getBody();
    
        if ($body->isSeekable()) {
            $body->rewind();
        }
    
        print $body->getContents();
        die;
    }
    
    /**
     * Output a caught exception to the client
     *
     * @param \Throwable $e
     */
    public function outputException(\Throwable $e) : void
    {
        $response = new Response(500);
        $response = $response->withAddedHeader('Content-Type', 'text/plain');
    
        if ($e instanceof HttpException) {
            $response = $response->withStatus($e->getCode());
        }
    
        $result = $e->getMessage() . "\n\r";
    
        if (ErrorHandler::$showTrace === true) {
            $trace = explode("\n", $e->getTraceAsString());
        
            foreach ($trace as $step) {
                $result .= "\n";
                $result .= $step;
            }
        }
    
        $content = new Stream(fopen('php://memory', 'r+b'));
        $content->write($result);
    
        $response = $response->withBody($content);
        
        $this->output($response);
    }
}