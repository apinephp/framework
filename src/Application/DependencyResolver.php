<?php
/**
 * DependencyInjector
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

namespace Apine\Application;

use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionParameter;

/**
 * This class resolve parameters into their corresponding value
 *
 * @package Apine\Application
 */
class DependencyResolver
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * @param ContainerInterface|null $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    /**
     * Compare a method parameter with a list of arguments and
     * a container to resolve it into its value/dependency
     *
     * @param ReflectionParameter $parameter
     *
     * @return mixed
     */
    public function resolve(ReflectionParameter $parameter)
    {
        $name = $parameter->getName();
        $value = $parameter->isDefaultValueAvailable() ?
            $parameter->getDefaultValue() :
            null;
        
        if (
            $this->container instanceof ContainerInterface &&
            $this->container->has($name)
        ) {
            $value = $this->container->get($name);
        }
        
        return $value;
    }
    
    /**
     * Execute a method definition against an object and a list
     * of named arguments that may be passed to the method
     * applying automatic dependency resolving and injection
     *
     * @param \ReflectionMethod $reflectionMethod
     * @param object|null       $object
     *
     * @return mixed
     */
    public function execute(ReflectionMethod $reflectionMethod, object $object = null)
    {
        $methodArguments = [];
        
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $methodArguments[$parameter->getName()] = $this->resolve($parameter);
        }
        
        if ($reflectionMethod->isConstructor()) {
            $reflectionClass = $reflectionMethod->getDeclaringClass();
            
            return $reflectionClass->newInstanceArgs($methodArguments);
        }
        
        return $reflectionMethod->invokeArgs($object, $methodArguments);
    }
    
    /**
     * Execute a function definition against a list
     * of named arguments that may be passed to the function
     * applying automatic dependency resolving and injection
     *
     * @param \ReflectionFunction $reflectionFunction
     *
     * @return mixed
     */
    public function call(ReflectionFunction $reflectionFunction)
    {
        $functionArguments = [];
        
        foreach ($reflectionFunction->getParameters() as $parameter) {
            $functionArguments[$parameter->getName()] = $this->resolve($parameter);
        }
        
        return $reflectionFunction->invokeArgs($functionArguments);
    }
    
    /**
     * Create an instance of a class applying automatic
     * dependency resolving and injection
     *
     * @param \ReflectionClass $reflectionClass
     *
     * @return mixed|object
     */
    public function createInstance(ReflectionClass $reflectionClass)
    {
        $reflectionMethod = $reflectionClass->getConstructor();
        
        if ($reflectionMethod instanceof ReflectionMethod) {
            return $this->execute($reflectionMethod);
        }
        
        return $reflectionClass->newInstance();
    }
}