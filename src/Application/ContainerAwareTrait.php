<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Application;

use Psr\Container\ContainerInterface;

/**
 * Trait ContainerAwareTrait
 *
 * @package Apine\Application
 */
trait ContainerAwareTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    
    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container) : void
    {
        $this->container = $container;
    }
}