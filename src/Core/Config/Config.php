<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Config;

use Apine\Core\Config\Handlers\ConfigHandlerInterface;

/**
 * Class Config
 *
 * @package Apine\Core\Config
 */
final class Config
{
    /**
     * @var ConfigHandlerInterface
     */
    private $handler;
    
    public function __construct(ConfigHandlerInterface $handler)
    {
        $this->handler = $handler;
    }
    
    public function __get($name)
    {
        return $this->handler->fetch($name);
    }
    
    public function __set($name, $value)
    {
        $this->handler->define($name, $value);
    }
    
    public function __isset($name)
    {
        return $this->handler->find($name);
    }
    
    public function __destruct()
    {
        $this->handler->commit();
    }
    
    public function save(): void
    {
        $this->handler->commit();
    }
}