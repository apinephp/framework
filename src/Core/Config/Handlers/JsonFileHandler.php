<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Config\Handlers;

use Apine\Core\Json\Json;
use InvalidArgumentException;

/**
 * Class FileHandler
 *
 * @package Apine\Core\Config\Handlers
 */
final class JsonFileHandler implements ConfigHandlerInterface
{
    /**
     * Path to the config file
     *
     * @var string
     */
    private $path;
    
    /**
     * @var Json
     */
    private $data;
    
    /**
     * @var Json
     */
    private $defaultState;
    
    /**
     * SingleFileJsonHandler constructor.
     *
     * @param string $path
     *
     * @throws \Apine\Core\Json\JsonInvalidFormatException
     */
    public function __construct(string $path)
    {
        $this->path = $path;
        
        if (file_exists($path)) {
            $this->data = new Json(file_get_contents($path));
            $this->defaultState = clone $this->data;
        } else {
            throw new InvalidArgumentException(sprintf('File %s not found', $path));
        }
    }
    
    /**
     * Returns an entry from the config
     *
     * @param string $name
     *
     * @return mixed
     */
    public function fetch(string $name)
    {
        return $this->find($name) ? $this->data->$name : null;
    }
    
    /**
     * Add or modify the value of an entry from the config
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return void
     */
    public function define(string $name, $value): void
    {
        $this->data->$name = $value;
    }
    
    /**
     * Check if an entry exists in the config
     *
     * Returns TRUE if the entry is found
     *
     * @param string $name
     *
     * @return bool
     */
    public function find(string $name): bool
    {
        return isset($this->data->$name);
    }
    
    /**
     * Writes the modifications to storage medium
     */
    public function commit(): void
    {
        $resource = fopen($this->path, 'w+b');
        fwrite($resource, (string) $this->data);
        fclose($resource);
        
        $this->defaultState = clone $this->data;
    }
    
    /**
     * Reverts the modifications since the last
     * commit or since the creation of the object
     * if no commits were performed
     */
    public function flush(): void
    {
        $this->data = clone $this->defaultState;
    }
}