<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Config\Handlers;

/**
 * Interface ConfigHandlerInterface
 *
 * @package Apine\Core\Config\Handlers
 */
interface ConfigHandlerInterface
{
    /**
     * Returns an entry from the config
     *
     * @param string $name
     *
     * @return mixed
     */
    public function fetch(string $name);
    
    /**
     * Add or modify the value of an entry from the config
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return void
     */
    public function define(string $name, $value): void;
    
    /**
     * Check if an entry exists in the config
     *
     * Returns TRUE if the entry is found
     *
     * @param string $name
     *
     * @return bool
     */
    public function find(string $name): bool;
    
    /**
     * Writes the modifications to storage medium
     */
    public function commit(): void;
    
    /**
     * Reverts the modifications since the last
     * commit or since the creation of the object
     * if no commits were performed
     */
    public function flush(): void;
}