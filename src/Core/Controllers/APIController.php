<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Controllers;

/**
 * Basic API Controller
 * Describes a reference controller for MVC pattern implementation
 * used for user-defined RESTful controllers
 *
 * @author Tommy Teasdale <tteasdaleroads@gmail.com>
 * @package Apine\Core\Controllers
 */
abstract class APIController extends Controller
{

}