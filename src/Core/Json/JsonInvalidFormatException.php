<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Json;

/**
 * Class JsonStoreFileNotFoundException
 *
 * @package Apine\Core\Json
 */
final class JsonInvalidFormatException extends \Exception
{
    
}