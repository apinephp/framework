<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */

namespace Apine\Core\Utility;

/**
 * Class StringManipulation
 *
 * @package Apine\Core\Utility
 */
class StringManipulation
{
    /**
     * A split method that supports unicode characters
     *
     * @param string  $a_string
     * @param integer $a_length
     *
     * @return string|array
     * @author qerery <qeremy@gmail.com>
     * @see http://us.php.net/str_split#107658
     */
    public static function splitUnicode($a_string, $a_length = 0)
    {
        if ($a_length > 0) {
            
            $ret = array();
            $a_lengthen = mb_strlen($a_string, "UTF-8");
            
            for ($i = 0; $i < $a_lengthen; $i += $a_length) {
                $ret[] = mb_substr($a_string, $i, $a_length, "UTF-8");
            }
            
            return $ret;
        }
        
        return preg_split("//u", $a_string, -1, PREG_SPLIT_NO_EMPTY);
    }
    
}