<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Error\Http;

use \Exception;
use Throwable;

/**
 * Class HttpException
 *
 * @package Apine\Core\Error\Http
 */
class HttpException extends Exception
{
    public function __construct(
        string $message,
        int $code = 500,
        Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }
}