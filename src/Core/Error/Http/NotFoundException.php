<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Error\Http;

use Throwable;

/**
 * Class NotFoundException
 *
 * @package Apine\Core\Error\Http
 */
final class NotFoundException extends HttpException
{
    public function __construct(
        string $message = 'Not Found',
        Throwable $previous = null
    )
    {
        parent::__construct($message, 404, $previous);
    }
}