<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Error\Http;

use Throwable;

/**
 * Class ForbiddenException
 *
 * @package Apine\Core\Error\Http
 */
final class ForbiddenException extends HttpException
{
    public function __construct(
        string $message = 'Forbidden',
        Throwable $previous = null
    )
    {
        parent::__construct($message, 403, $previous);
    }
}