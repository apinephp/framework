<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views;

use Apine\Http\Stream;
use Psr\Http\Message\ResponseInterface;

/**
 * Class JSONView
 *
 * @package Apine\Core\Views
 */
final class JSONView extends View
{
    use InjectableDataTrait;
    
    private $prettify = false;
    
    public function prettify(bool $prettify): void
    {
        $this->prettify = $prettify;
    }
    
    public function respond(array $data = [], bool $prettify = false, int $code = 200): ResponseInterface
    {
    
        if ($data !== null) {
            $this->attributes = $data;
        }
    
        $this->prettify($prettify);
        $this->setStatusCode($code);
        
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
        $response = $response->withHeader('content-type', 'application/json');
        $options = 0;
    
        if (true === $this->prettify){
            $options |= JSON_PRETTY_PRINT;
        }
    
        $json = json_encode($this->getAttributes(), $options);
    
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
    
        $body = new Stream(fopen('php://memory', 'r+b'));
        $body->write($json);
        $response = $response->withBody($body);
        
        return $response;
    }
}