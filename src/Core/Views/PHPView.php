<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views;

use Apine\Http\Stream;
use Psr\Http\Message\ResponseInterface;

/**
 * Class PHPView
 *
 * @package Apine\Core\Views
 */
final class PHPView extends View
{
    use InjectableDataTrait;
    
    /**
     * Path to layout file
     *
     * @var string
     */
    private $file;
    
    public function respond(string $template = null, array $data = null, int $code = 200): ResponseInterface
    {
        if ($template !== null) {
            $this->setFile($template);
        }
    
        if ($data !== null) {
            $this->attributes = $data;
        }
    
        $this->setStatusCode($code);
    
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
        $response = $response->withHeader('content-type', 'text/html');
        
        if ($this->file !== null) {
            $body = new Stream(fopen('php://memory', 'r+b'));
            $body->write(self::generate($this->file, $this->attributes));
            $response = $response->withBody($body);
        } else {
            throw new \InvalidArgumentException('Missing Template');
        }
    
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
        
        return $response;
    }
    
    public function setFile(string $path): void
    {
        if (file_exists("$path.php")) {
            $path = "$path.php";
        } else {
            throw new \InvalidArgumentException('File not found');
        }
        
        $this->file = $path;
    }
    
    /**
     * @param string $layoutFile
     * @param array  $layoutAttributes
     *
     * @return string
     * @throws \Throwable
     */
    private static function generate (string $layoutFile, array $layoutAttributes) : string
    {
        try {
            extract($layoutAttributes, EXTR_OVERWRITE);
            /** @noinspection UselessUnsetInspection */
            unset($layoutAttributes);
        
            ob_start();
            eval("unset(\$layoutFile); include_once('".$layoutFile."');");
            $content = ob_get_contents();
            ob_end_clean();
            
            return $content;
        } catch (\Throwable $e) {
            ob_end_clean();
            throw $e;
        }
    }
}