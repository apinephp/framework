<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views;

use Apine\Http\Stream;
use Psr\Http\Message\ResponseInterface;
use function in_array;

/**
 * Class FileView
 *
 * @package Apine\Core\Views
 */
final class FileView extends View
{
    /**
     * @var resource
     */
    private $resource;
    
    /**
     * @var string
     */
    private $path;
    
    public function respond(string $file = null, int $code = 200): ResponseInterface
    {
        if ($file !== null) {
            $this->setFile($file);
        }
        
        if (null === $this->resource) {
            throw new \InvalidArgumentException('Missing File to output');
        }
        
        $this->setStatusCode($code);
        
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
        
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
        
        $response = $response->withBody(new Stream($this->resource));
        
        return $response;
    }
    
    public function setFile(string $file): void
    {
        if (!file_exists($file)) {
            throw new \InvalidArgumentException(sprintf('File %s not found', $file));
        }
        
        $this->resource = fopen($file, 'rb');
        $this->path = $file;
        
        $this->addHeader('Content-Type', self::fileType($this->path));
        $this->addHeader('Content-Length', filesize($this->path));
    }
    
    private static function fileType(string $path): string
    {
        $mime = '';
        
        if (class_exists('finfo')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $path);
        } elseif (!strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $filename = escapeshellcmd($path);
            $mime = shell_exec("file -b --mime-type '" . $filename . "'");
        } elseif (self::isExecAvailable()) {
            $filename = escapeshellcmd($path);
            $mime = exec("file -b --mime-type '" . $filename . "'");
        }
        
        return $mime;
    }
    
    /**
     * Verify if exec is disabled
     *
     * @author Daniel Convissor
     * @see http://stackoverflow.com/questions/3938120/check-if-exec-is-disabled
     */
    private static function isExecAvailable(): bool
    {
        static $available;
        
        if ($available === null) {
            $available = true;
            $d = ini_get('disable_functions');
            $s = ini_get('suhosin.executor.func.blacklist');
            if ("$d$s") {
                $array = preg_split('/,\s*/', "$d,$s");
                if (in_array('exec', $array, true)) {
                    $available = false;
                }
            }
        }
        
        return $available;
    }
}