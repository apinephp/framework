<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views;

use Apine\Http\Stream;
use DOMDocument;
use DOMNode;
use Psr\Http\Message\ResponseInterface;

/**
 * Class XMLView
 *
 * @package Apine\Core\Views
 */
final class XMLView extends View
{
    
    /**
     * @var \DOMDocument
     */
    private $document;
    
    public function __construct(ResponseInterface $response, DOMDocument $document = null)
    {
        parent::__construct($response);
        
        $this->document = $document ?? new DOMDocument('1.0', 'UTF-8');
    }
    
    public function loadString(string $xml): bool
    {
        try {
            return $this->document->loadXML($xml);
        } catch (\Throwable $e) {
            throw new \InvalidArgumentException('Could not load XML from string', 500, $e);
        }
    }
    
    public function loadFile(string $path): bool
    {
        if (!file_exists($path)) {
            throw new \InvalidArgumentException(sprintf('File %s not found', $path));
        }
        
        try {
            return $this->document->load($path);
        } catch (\Throwable $e) {
            throw new \InvalidArgumentException(sprintf('Could not load XML from file %s', $path), 500, $e);
        }
    }
    
    /**
     * Produce a response
     *
     * @param \DOMNode|null $node
     *      Use this parameter to output only a specific node without XML declaration rather than the entire document.
     * @param int           $code
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function respond(DOMNode $node = null, int $code = 200): ResponseInterface
    {
        if ($node === null && !($this->document->documentElement instanceof DOMNode)) {
            throw new \InvalidArgumentException("Cannot generate empty xml document");
        }
        
        $this->setStatusCode($code);
        
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
        $response = $response->withAddedHeader('content-type', 'text/xml');
        
        $body = new Stream(fopen('php://memory', 'r+b'));
    
        $bodyDocument = clone $this->document;
    
        if ($node !== null) {
            if ($node instanceof DOMDocument) {
                $string = $node->saveXML();
            } else {
                $node = $bodyDocument->importNode($node, true);
                $string = $bodyDocument->saveXML($node);
            }
        } else {
            $string = $bodyDocument->saveXML();
        }
    
        $body->write($string);
        
        $response = $response->withBody($body);
    
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
        
        return $response;
    }
}