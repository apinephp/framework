<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views\Twig\Extensions;

use function Apine\Core\Utility\executionTime;

/**
 * Class ExcecutionTimeExtension
 *
 * @package Apine\Core\Views\Twig
 */
class ExecutionTimeExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('execution_time', function () {
                return executionTime();
            })
        );
    }
}