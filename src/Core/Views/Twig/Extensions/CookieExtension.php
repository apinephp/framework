<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views\Twig\Extensions;

/**
 * Class CookieExtension
 *
 * @package Apine\Core\Views\Twig
 */
class CookieExtension extends \Twig_Extension
{
    function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('cookie', function(string $name): string {
                return $_COOKIE[$name];
            })
        ];
    }
}