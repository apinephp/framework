<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views\Twig;

use Apine\Application\DependencyResolver;
use ReflectionClass;
use ReflectionFunction;
use Twig_Environment;
use Twig_Extension;
use function is_string, is_callable;

/**
 * Class ExtensionLoader
 *
 * @package Apine\Core\Views\Twig
 */
class ExtensionLoader
{
    private $environment;
    
    private $resolver;
    
    public function __construct(Twig_Environment $environment, DependencyResolver $resolver)
    {
        $this->environment = $environment;
        $this->resolver = $resolver;
    }
    
    /**
     * @param string|callable|Twig_Extension $extension
     *
     * @return \Twig_Environment
     * @throws \ReflectionException
     */
    public function addExtension($extension): Twig_Environment
    {
        if (is_string($extension) && !is_callable($extension)) {
            $reflection = new ReflectionClass($extension);
            $this->environment->addExtension($this->resolver->createInstance($reflection));
        } else if (is_callable($extension)) {
            $reflection = new ReflectionFunction($extension);
            $this->environment->addExtension($this->resolver->call($reflection));
        } else if ($extension instanceof Twig_Extension) {
            $this->environment->addExtension($extension);
        } else {
            throw new \InvalidArgumentException('Extension should be instance of Twig_Extension');
        }
        
        return $this->environment;
    }
}