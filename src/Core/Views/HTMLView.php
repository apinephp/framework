<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

namespace Apine\Core\Views;

use Apine\Http\Stream;
use Psr\Http\Message\ResponseInterface;
use Twig_Environment;
use Twig_Loader_Filesystem;
use function dirname;

/**
 * Class HTMLView
 *
 * @package Apine\Core\Views
 */
final class HTMLView extends View
{
    use InjectableDataTrait;
    
    /**
     * Path to layout file
     *
     * @var string
     */
    private $file;
    
    /**
     * @var string
     */
    private $filePath;
    
    /**
     * @var \Twig_Environment
     */
    private $twig;
    
    public function __construct(ResponseInterface $response, Twig_Environment $twig)
    {
        parent::__construct($response);
        $this->twig = $twig;
    }
    
    /**
     * @param string|null $template
     * @param array|null  $data
     * @param int         $code
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function respond(string $template = null, array $data = null, int $code = 200): ResponseInterface
    {
        if ($template !== null) {
            $this->setFile($template);
        }
        
        if ($data !== null) {
            $this->attributes = $data;
        }
        
        $this->setStatusCode($code);
        
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
        $response = $response->withHeader('content-type', 'text/html');
    
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
        
        if ($this->file !== null) {
            $twig = clone $this->twig;
            $loader = new Twig_Loader_Filesystem($this->filePath);
            $twig->setLoader($loader);
            
            $content = $twig->render($this->file, $this->attributes);
            
            $body = new Stream(fopen('php://memory', 'r+b'));
            $body->write($content);
            $response = $response->withBody($body);
        } else {
            throw new \InvalidArgumentException('Missing Template');
        }
        
        return $response;
    }
    
    public function setFile(string $path): void
    {
        if (file_exists("$path.twig")) {
            $filePath = realpath(dirname("$path.twig"));
            $file = basename("$path.twig");
        } else if (file_exists("$path.html")) {
            $filePath = realpath(dirname("$path.html"));
            $file = basename("$path.html");
        } else {
            throw new \InvalidArgumentException('File not found');
        }
        
        $this->file = $file;
        $this->filePath = $filePath;
    }
}