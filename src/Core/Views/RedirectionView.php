<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);


namespace Apine\Core\Views;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use function is_string;

/**
 * Class RedirectionView
 *
 * @package Apine\Core\Views
 */
final class RedirectionView extends View
{
    /**
     * @var \Psr\Http\Message\UriInterface
     */
    private $uri;
    
    /**
     * @var \Psr\Http\Message\UriFactoryInterface
     */
    private $uriFactory;
    
    public function __construct(ResponseInterface $response, UriFactoryInterface $factory)
    {
        parent::__construct($response);
        $this->uriFactory = $factory;
    }
    
    public function setUri($uri): void
    {
        if (is_string($uri)) {
            $this->uri = $this->uriFactory->createUri($uri);
        } else if ($uri instanceof UriInterface) {
                $this->uri = $uri;
        } else {
            throw new \InvalidArgumentException('Invalid URI');
        }
    }
    
    /**
     * @param null|string|UriInterface $uri
     * @param int                      $code
     *
     * @return ResponseInterface
     */
    public function respond($uri = null, int $code = 301): ResponseInterface
    {
        if ($uri !== null) {
            $this->setUri($uri);
        }
        
        if ($this->uri === null) {
            throw new \InvalidArgumentException('Missing URI to redirect to');
        }
        
        $this->setStatusCode($code);
        
        $response = clone $this->response;
        $response = $response->withStatus($this->statusCode);
    
        foreach ($this->headers as $header) {
            $response = $response->withHeader($header['name'], $header['value']);
        }
        
        return $response->withHeader('Location', (string)$this->uri);
    }
}