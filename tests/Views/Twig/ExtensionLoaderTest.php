<?php
/**
 * ExtensionLoaderTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

use Apine\Application\DependencyResolver;
use Apine\Core\Views\Twig\ExtensionLoader;
use Apine\Core\Views\View;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\LoaderInterface;

class ExtensionLoaderTest extends TestCase
{
    public function testConstructor(): void
    {
        $twigloader = $this->getMockBuilder(LoaderInterface::class)->getMock();
        
        /** @var \Twig\Environment $observer */
        $observer = $this->getMockBuilder(Environment::class)
            ->setConstructorArgs([
                'loader' => $twigloader
            ])
            ->setMethods(['addExtension'])
            ->getMock();
        
        /** @var DependencyResolver $resolver */
        $resolver = $this->createMock(DependencyResolver::class);
        
        $loader = new ExtensionLoader($observer, $resolver);
        
        $this->assertAttributeInstanceOf(Environment::class, 'environment', $loader);
        $this->assertAttributeInstanceOf(DependencyResolver::class, 'resolver', $loader);
    }
    
    public function testAddExtensionWhenInstanceOfExtension(): void
    {
        $twigloader = $this->getMockBuilder(LoaderInterface::class)->getMock();
    
        /** @var \Twig\Environment $environment */
        $environment = $this->getMockBuilder(Environment::class)
            ->setConstructorArgs([
                'loader' => $twigloader
            ])
            ->setMethods(['addExtension'])
            ->getMock();
        
        //$extension = $this->getMockBuilder(Twig_Extension::class)->getMockForAbstractClass();
        $extension = $this->getMockForAbstractClass(Twig_Extension::class);
    
        /** @var DependencyResolver $resolver */
        $resolver = $this->createMock(DependencyResolver::class);
        
        $loader = new ExtensionLoader($environment, $resolver);
    
        $twig = $loader->addExtension($extension);
        $this->assertInstanceOf(Environment::class, $twig);
    }
    
    public function testAddExtensionWhenString(): void
    {
        $twigloader = $this->getMockBuilder(LoaderInterface::class)->getMock();
    
        /** @var \Twig\Environment $environment */
        $environment = $this->getMockBuilder(Environment::class)
            ->setConstructorArgs([
                'loader' => $twigloader
            ])
            ->setMethods(['addExtension'])
            ->getMock();
    
        /** @var Twig_Extension $extension */
        $extension = $this->getMockForAbstractClass(Twig_Extension::class);
        
        /** @var DependencyResolver|MockObject $resolver */
        $resolver = $this->getMockBuilder(DependencyResolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['createInstance'])
            ->getMock();
        $resolver->method('createInstance')->willReturnCallback(function () use ($extension) {
            return $extension;
        });
        
        $loader = new ExtensionLoader($environment, $resolver);
    
        $twig = $loader->addExtension(get_class($extension));
        $this->assertInstanceOf(Environment::class, $twig);
    }
    
    public function testAddExtensionWhenCallable(): void
    {
        $twigloader = $this->getMockBuilder(LoaderInterface::class)->getMock();
    
        /** @var \Twig\Environment $environment */
        $environment = $this->getMockBuilder(Environment::class)
            ->setConstructorArgs([
                'loader' => $twigloader
            ])
            ->setMethods(['addExtension'])
            ->getMock();
    
        /** @var Twig_Extension $extension */
        $extension = $this->getMockForAbstractClass(Twig_Extension::class);
    
        $callable = function () use ($extension) {
            return $extension;
        };
    
        /** @var DependencyResolver|MockObject $resolver */
        $resolver = $this->getMockBuilder(DependencyResolver::class)
            ->disableOriginalConstructor()
            ->setMethods(['call'])
            ->getMock();
        $resolver->method('call')->willReturnCallback(function () use ($extension) {
            return $extension;
        });
    
        $loader = new ExtensionLoader($environment, $resolver);
    
        $twig = $loader->addExtension($callable);
        $this->assertInstanceOf(Environment::class, $twig);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Extension should be instance of Twig_Extension
     */
    public function testAddExtensionWhenInvalidType()
    {
        $twigloader = $this->getMockBuilder(LoaderInterface::class)->getMock();
    
        /** @var \Twig\Environment $environment */
        $environment = $this->getMockBuilder(Environment::class)
            ->setConstructorArgs([
                'loader' => $twigloader
            ])
            ->setMethods(['addExtension'])
            ->getMock();
    
        
        $extension = $this->getMockBuilder(View::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
    
        /** @var DependencyResolver|MockObject $resolver */
        $resolver = $this->getMockBuilder(DependencyResolver::class)
            ->disableOriginalConstructor()
            ->getMock();
    
        $loader = new ExtensionLoader($environment, $resolver);
        
        $twig = $loader->addExtension($extension);
    }
}

class StubExtension extends Twig_Extension {}
