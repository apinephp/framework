<?php
/**
 * FileViewTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
/** @noinspection PhpUnusedLocalVariableInspection */
declare(strict_types=1);

use Apine\Core\Views\FileView;
use Apine\Http\Response;
use PHPUnit\Framework\TestCase;

class FileViewTest extends TestCase
{
    private static $filename = 'response.txt';
    
    /**
     * @beforeClass
     */
    public static function createFile(): void
    {
        $resource = fopen(self::$filename, 'w+b');
        fwrite($resource, 'This is a text file.');
        fclose($resource);
    }
    
    /**
     * @afterClass
     */
    public static function deleteFile(): void
    {
        unlink(self::$filename);
    }
    
    public function testSetFile(): void
    {
        $view = new FileView(new Response());
        $view->setFile(self::$filename);
    
        $this->assertAttributeInternalType('resource', 'resource', $view);
        $this->assertAttributeEquals(self::$filename, 'path', $view);
    
        $this->assertEquals('text/plain', $this->getObjectAttribute($view, 'headers')['content-type']['value']);
        $this->assertEquals(20, $this->getObjectAttribute($view, 'headers')['content-length']['value']);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /File (.*?) not found/
     */
    public function testSetFileFileNotExists(): void
    {
        $view = new FileView(new Response());
        $view->setFile('notfound.jpg');
    }
    
    public function testRespond(): void
    {
        $view = new FileView(new Response());
        $response = $view->respond(self::$filename);
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertTrue($response->hasHeader('content-length'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Missing File to output
     */
    public function testRespondFileNotSet(): void
    {
        $view = new FileView(new Response());
        $response = $view->respond();
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /File (.*?) not found/
     */
    public function testRespondFileNotExists(): void
    {
        $view = new FileView(new Response());
        $response = $view->respond('notfound.jpg');
    }
    
    public function testRespondDoesIncludeCustomHeader(): void
    {
        $view = new FileView(new Response());
        $view->setFile(self::$filename);
        $view->addHeader('my-header', 'value');
        $response = $view->respond();
    
        $this->assertTrue($response->hasHeader('my-header'));
        $this->assertEquals('value', $response->getHeaderLine('my-header'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testRespondTemplateInvalidCode(): void
    {
        $view = new FileView(new Response());
        $response = $view->respond(self::$filename, 640);
    }
}
