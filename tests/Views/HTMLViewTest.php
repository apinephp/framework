<?php
/**
 * HTMLViewTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpIncompatibleReturnTypeInspection */
declare(strict_types=1);

use Apine\Core\Error\ErrorHandler;
use Apine\Core\Views\HTMLView;
use Apine\Http\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Twig\Environment;

class HTMLViewTest extends TestCase
{
    private static $filename = 'response';
    
    public function setUp()
    {
        ErrorHandler::set(); // Set up error handling so that notice in file manipulation convert to Exception
    }
    
    public function tearDown()
    {
        ErrorHandler::unset();
    }
    
    /**
     * @beforeClass
     */
    public static function createFile(): void
    {
        $resource = fopen(self::$filename . '.html', 'w+b');
        fwrite($resource, '<h1>Header in HTML</h1>');
        fclose($resource);
    
        mkdir('config');
    }
    
    /**
     * @afterClass
     */
    public static function deleteFile(): void
    {
        if (file_exists(self::$filename . '.html')) {
            unlink(self::$filename . '.html');
        }
        
        if (file_exists('config/twig.json')) {
            unlink('config/twig.json');
        }
    
        rmdir('config');
    }
    
    public function testConstructor(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
        
        /** @var \Twig_Environment $twig */
        $twig = $this->getMockBuilder(Environment::class)->disableOriginalConstructor()->getMock();
        
        //$view = new HTMLView(self::$filename);
        $view = new HTMLView($response, $twig);
        
        $this->assertAttributeNotEmpty('twig', $view);
    }
    
    public function mockTwig(): Environment
    {
        $mock = $this->getMockBuilder(Environment::class)
            ->disableOriginalConstructor()->getMock();
        
        $mock->method('render')
            ->willReturn('');
        
        return $mock;
    }
    
    public function testSetFile(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
    
        /** @var \Twig_Environment $twig */
        $twig = $this->getMockBuilder(Twig_Environment::class)->disableOriginalConstructor()->getMock();
    
        $view = new HTMLView($response, $twig);
    
        $resource = fopen(self::$filename . '.twig', 'w+b');
        fwrite($resource, '<h1>Header in HTML through Twig</h1>');
        fclose($resource);
        
        $view->setFile(self::$filename);
        
        $this->assertAttributeEquals(self::$filename . '.twig', 'file', $view);
    
        unlink(self::$filename . '.twig');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage File not found
     */
    public function testSetFileFileNotFound(): void
    {
        /** @var ResponseInterface $response */
        $response = $this->getMockForAbstractClass(ResponseInterface::class);
    
        /** @var \Twig_Environment $twig */
        $twig = $this->getMockBuilder(Twig_Environment::class)->disableOriginalConstructor()->getMock();
        
        $view = new HTMLView($response, $twig);
        $view->setFile('notfound');
    }
    
    public function testRespond(): void
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->mockTwig();
    
        $view = new HTMLView(new Response(), $twig);
        $response = $view->respond(self::$filename, ['name' => 'value']);
    
        $this->assertEquals('text/html', $response->getHeaderLine('content-type'));
    }
    
    public function testRespondDoesIncludeCustomHeader(): void
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->mockTwig();
    
        $view = new HTMLView(new Response(), $twig);
        $view->addHeader('my-header', 'value');
        $response = $view->respond(self::$filename);
    
        $this->assertTrue($response->hasHeader('my-header'));
        $this->assertEquals('value', $response->getHeaderLine('my-header'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Missing Template
     */
    public function testRespondTemplateNotSet(): void
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->mockTwig();
    
        $view = new HTMLView(new Response(), $twig);
        $response = $view->respond();
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage File not found
     */
    public function testRespondTemplateNotFound(): void
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->mockTwig();
    
        $view = new HTMLView(new Response(), $twig);
        $response = $view->respond('notFound');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testRespondTemplateInvalidCode(): void
    {
        /** @var \Twig_Environment $twig */
        $twig = $this->mockTwig();
        
        $view = new HTMLView(new Response(), $twig);
        $response = $view->respond(self::$filename, null, 640);
    }
}
