<?php
/**
 * URLHelperTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

use Apine\Core\Views\URLHelper;
use Apine\Http\Factories\UriFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriInterface;
use const \Apine\Core\PROTOCOL_HTTP;
use const \Apine\Core\PROTOCOL_HTTPS;

class URLHelperTest extends TestCase
{
    private static $server = [
        'HTTPS' => 'on',
        'REQUEST_METHOD' => 'GET',
        'SERVER_PROTOCOL' => 'HTTP/1.1',
        'HTTP_HOST' => 'example.com',
        'REQUEST_URI' => '/test/example?home=cat',
        'QUERY_STRING' => 'home=cat',
        'SERVER_NAME' => 'example.com',
        'SERVER_ADDR' => '127.0.0.1',
        'SERVER_PORT' => 443
    ];
    
    public function testConstructor(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertAttributeInstanceOf(UriInterface::class, 'uri', $helper);
        $this->assertAttributeEquals('/test/example', 'path', $helper);
        $this->assertAttributeEquals('example.com', 'mainAuthority', $helper);
        $this->assertAttributeEquals('example.com', 'authority', $helper);
    }
    
    public function testConstructorHostHasSubDomain(): void
    {
        $server = self::$server;
        $server['HTTP_HOST'] = 'test.example.com';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
    
        $this->assertAttributeEquals('example.com', 'mainAuthority', $helper);
        $this->assertAttributeEquals('test.example.com', 'authority', $helper);
    }
    
    public function testPath(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/path', $helper->path('test/path'));
    }
    
    public function testPathForceHTTP(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('http://example.com/test/path', $helper->path('test/path', PROTOCOL_HTTP));
    }
    
    public function testPathForceHTTPS(): void
    {
        $server = self::$server;
        $server['HTTPS'] = 'off';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/path', $helper->path('test/path', PROTOCOL_HTTPS));
    }
    
    public function testResource(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/path.txt', $helper->resource('test/path.txt'));
    }
    
    public function testRelativePath(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/example/test/path', $helper->relativePath('test/path'));
    }
    
    public function testRelativePathForceHTTP(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('http://example.com/test/example/test/path', $helper->relativePath('test/path', PROTOCOL_HTTP));
    }
    
    public function testRelativePathForceHTTPS(): void
    {
        $server = self::$server;
        $server['HTTPS'] = 'off';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/example/test/path', $helper->relativePath('test/path', PROTOCOL_HTTPS));
    }
    
    public function testMainPath(): void
    {
        $server = self::$server;
        $server['HTTP_HOST'] = 'test.example.com';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
    
        $this->assertEquals('https://example.com/test/main', $helper->mainPath('test/main'));
    }
    
    public function testMainPathForceHTTP(): void
    {
        $server = self::$server;
        $server['HTTP_HOST'] = 'test.example.com';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('http://example.com/test/main', $helper->mainPath('test/main', PROTOCOL_HTTP));
    }
    
    public function testMainPathForceHTTPS(): void
    {
        $server = self::$server;
        $server['HTTPS'] = 'off';
        $server['HTTP_HOST'] = 'test.example.com';
    
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray($server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/main', $helper->mainPath('test/main', PROTOCOL_HTTPS));
    }
    
    public function testGetCurrentPath(): void
    {
        $factory = new UriFactory();
        $uri = $factory->createUriFromArray(self::$server);
        $helper = new URLHelper($uri);
        
        $this->assertEquals('https://example.com/test/example', $helper->getCurrentPath());
    }
}
