<?php
/**
 * RedirectionViewTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

use Apine\Http\Factories\UriFactory;
use Apine\Core\Views\RedirectionView;
use Apine\Http\Response;
use Apine\Http\Uri;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriInterface;

class RedirectionViewTest extends TestCase
{
    public function testSetUriFromString(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $redirection->setUri('https://google.ca');
    
        $this->assertAttributeInstanceOf(UriInterface::class, 'uri', $redirection);
        $this->assertEquals('https://google.ca', (string) $this->getObjectAttribute($redirection, 'uri'));
    }
    
    public function testSetUriFromUri(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $redirection->setUri(new Uri('https://google.ca'));
    
        $this->assertAttributeInstanceOf(UriInterface::class, 'uri', $redirection);
        $this->assertEquals('https://google.ca', (string) $this->getObjectAttribute($redirection, 'uri'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid URI
     */
    public function testSetUriInvalidType(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $redirection->setUri(8954698);
    }
    
    public function testRespond(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $response = $redirection->respond('/home', 301);
        
        $this->assertEquals('/home', $response->getHeaderLine('location'));
        $this->assertEquals(301, $response->getStatusCode());
    }
    
    public function testRespondDoesIncludeCustomHeader(): void
    {
        $view = new RedirectionView(new Response(), new UriFactory());
        $view->addHeader('my-header', 'value');
        $response = $view->respond('/home');
        
        $this->assertTrue($response->hasHeader('my-header'));
        $this->assertEquals('value', $response->getHeaderLine('my-header'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid URI
     */
    public function testRespondInvalidUri(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $response = $redirection->respond(301);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Missing URI to redirect to
     */
    public function testRespondNoUri(): void
    {
        $redirection = new RedirectionView(new Response(), new UriFactory());
        $response = $redirection->respond();
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testRespondTemplateInvalidCode(): void
    {
        $view = new RedirectionView(new Response(), new UriFactory());
        $response = $view->respond('https://google.ca', 640);
    }
}
