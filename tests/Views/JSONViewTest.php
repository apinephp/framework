<?php
/**
 * JSONViewTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */
declare(strict_types=1);

use Apine\Core\Views\JSONView;
use Apine\Http\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class JSONViewTest extends TestCase
{
    static private $array = [
        'name' => 'value',
        'array' => [
            1, 2, 3
        ]
    ];
    
    public function testRespond(): void
    {
        $view = new JSONView(new Response());
        $response = $view->respond(self::$array);
        
        $body = $response->getBody();
        $body->rewind();
        $content = $body->getContents();
        
        $this->assertEquals('application/json', $response->getHeaderLine('content-type'));
        $this->assertNotEmpty($content);
        $this->assertFalse(strpos($content, PHP_EOL));
    }
    
    public function testRespondPrettify(): void
    {
        $view = new JSONView(new Response());
        $response = $view->respond(self::$array, true);
    
        $body = $response->getBody();
        $body->rewind();
        $content = $body->getContents();
    
        $this->assertNotFalse(strpos($content, PHP_EOL));
    }
    
    public function testRespondDoesIncludeCustomHeader(): void
    {
        $view = new JSONView(new Response());
        $view->addHeader('my-header', 'value');
        $response = $view->respond(self::$array);
    
        $this->assertTrue($response->hasHeader('my-header'));
        $this->assertEquals('value', $response->getHeaderLine('my-header'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testRespondTemplateInvalidCode(): void
    {
        $view = new JSONView(new Response());
        $response = $view->respond(self::$array, false, 640);
    }
}
