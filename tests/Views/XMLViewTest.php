<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
declare(strict_types=1);

use Apine\Core\Views\XMLView;
use Apine\Http\Response;
use PHPUnit\Framework\TestCase;

class XMLViewTest extends TestCase
{
    static private $xmlString = '<?xml version="1.0"?>
<book>
  <title>This is the title</title>
</book>';
    
    static private $xmlFileString = '<?xml version="1.0"?>
<movie>
  <title>This is the title</title>
</movie>';
    
    static private $filename = 'xmlfile.xml';
    
    /**
     * @beforeClass
     */
    public static function createFile(): void
    {
        $resource = fopen(self::$filename, 'w+b');
        fwrite($resource, self::$xmlFileString);
        fclose($resource);
        
        $resource = fopen(self::$filename . '_bad', 'w+b');
        fwrite($resource, '{"a":"b"}');
        fclose($resource);
    }
    
    /**
     * @afterClass
     */
    public static function deleteFile(): void
    {
        unlink(self::$filename);
        unlink(self::$filename . '_bad');
    }
    
    public function testLoadFile(): void
    {
        $view = new XMLView(new Response());
        $this->assertTrue($view->loadFile(self::$filename));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /File (.*?) not found/
     */
    public function testLoadFileFileNotFound(): void
    {
        $view = new XMLView(new Response());
        $view->loadFile('notfound');
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /Could not load XML from file (.*?)/
     */
    public function testLoadFileInvalidContents(): void
    {
        $view = new XMLView(new Response());
        $view->loadFile(self::$filename . '_bad');
    }
    
    public function testLoadString(): void
    {
        $view = new XMLView(new Response());
        $this->assertTrue($view->loadString(self::$xmlString));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Could not load XML from string
     */
    public function testLoadStringInvalidContents(): void
    {
        $view = new XMLView(new Response());
        $view->loadString('{"a": "b"}');
    }
    
    public function testRespondWhenLoadFromString(): void
    {
        $view = new XMLView(new Response());
        $view->loadString(self::$xmlString);
        
        $response = $view->respond();
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
        
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlString(self::$xmlString, $string);
    }
    
    public function testRespondWhenLoadFromFile(): void
    {
        $view = new XMLView(new Response());
        $view->loadFile(self::$filename);
    
        $response = $view->respond();
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlFile(self::$filename, $string);
    }
    
    public function testRespondWhenProvidingNode(): void
    {
        $validation_string = '<?xml version="1.0" encoding="UTF-8"?><element>text value</element>';
        $view = new XMLView(new Response());
        $node = new \DOMElement('element', 'text value');
    
        $response = $view->respond($node);
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlString($validation_string, $string);
    }
    
    public function testRespondWhenProvidingDocument(): void
    {
        $validation_string = '<?xml version="1.0" encoding="UTF-8"?><element>text value</element>';
        $view = new XMLView(new Response());
        $node = new DOMDocument('1.0', 'UTF-8');
        $node->appendChild($node->createElement('element', 'text value'));
        
        $response = $view->respond($node);
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
        
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlString($validation_string, $string);
    }
    
    public function testRespondWhenLoadFromStringOrFileThenProvidingNode(): void
    {
        $validation_string = '<?xml version="1.0" encoding="UTF-8"?><element>text value</element>';
        $view = new XMLView(new Response());
        $node = new \DOMElement('element', 'text value');
        
        $view->loadFile(self::$filename);
    
        $response = $view->respond($node);
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlString($validation_string, $string);
    }
    
    public function testRespondWhenLoadFromStringThenLoadFromFile(): void
    {
        $view = new XMLView(new Response());
        $view->loadString(self::$xmlString);
        $view->loadFile(self::$filename);
    
        $response = $view->respond();
        $response->getBody()->rewind();
        $string = $response->getBody()->getContents();
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('text/xml', $response->getHeaderLine('content-type'));
        $this->assertXmlStringEqualsXmlFile(self::$filename, $string);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Cannot generate empty xml document
     */
    public function testRespondWhenNoLoadFromStringNorFileNorProvidingNode (): void
    {
        $view = new XMLView(new Response());
        $response = $view->respond();
    }
    
    public function testRespondWhenOverwritingContentTypeHeader(): void
    {
        $view = new XMLView(new Response());
        $view->loadString(self::$xmlString);
        $view->addHeader('content-type', 'application/xml');
    
        $response = $view->respond();
    
        $this->assertTrue($response->hasHeader('content-type'));
        $this->assertEquals('application/xml', $response->getHeaderLine('content-type'));
    }
    
    public function testRespondDoesIncludeCustomHeader(): void
    {
        $view = new XMLView(new Response());
        $view->loadString(self::$xmlString);
        $view->addHeader('my-header', 'value');
        $response = $view->respond();
    
        $this->assertTrue($response->hasHeader('my-header'));
        $this->assertEquals('value', $response->getHeaderLine('my-header'));
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Invalid HTTP status code
     */
    public function testResponseInvalidCode(): void
    {
        $view = new XMLView(new Response());
        $view->loadFile(self::$filename);
        $response = $view->respond(null, 640);
    }
}
