<?php
/**
 * DependencyResolverTest
 *
 * @license MIT
 * @copyright 2018 Tommy Teasdale
 */

/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection ReturnTypeCanBeDeclaredInspection */
/** @noinspection PhpUnusedLocalVariableInspection */

declare(strict_types=1);

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Apine\Application\DependencyResolver;
use PHPUnit\Framework\TestCase;

class DependencyResolverTest extends TestCase
{
    public function testResolve()
    {
        $pimple = new Pimple\Container();
        $pimple['response'] = function () {
            return $this->getMockForAbstractClass(ResponseInterface::class);
        };
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
        
        $resolver = new DependencyResolver($container);
        
        $reflectionFunction = new ReflectionFunction('DependencyResolverFunctionCall');
        $parameter = $reflectionFunction->getParameters()[0];
        $value = $resolver->resolve($parameter);
        $this->assertInstanceOf(ResponseInterface::class, $value);
    }
    
    public function testResolveDependencyWhenTypeNotDefined()
    {
        $pimple = new Pimple\Container();
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
    
        $resolver = new DependencyResolver($container);
        
        $reflectionMethod = new ReflectionMethod(DependencyResolverTestClassNoConstructor::class, 'inputTest');
        $parameter = $reflectionMethod->getParameters()[0];
        $value = $resolver->resolve($parameter);
        $this->assertInstanceOf(ServerRequestInterface::class, $value);
    }
    
    public function testResolveDependencyNotFound()
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['get', 'has'])
            ->getMockForAbstractClass();
        $container->expects($this->any())->method('has')->with($this->equalTo('request'))->will($this->returnValue(false));
        
        $resolver = new DependencyResolver($container);
        
        $class = new ReflectionClass(DependencyResolverTestClass::class);
        $constructor = $class->getConstructor();
        
        $parameter = $constructor->getParameters()[0];
        $value = $resolver->resolve($parameter);
        $this->assertNull($value);
    }
    
    public function testResolveDependencyNotFoundDefaultValue()
    {
        /** @var MockObject|ContainerInterface $container */
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->setMethods(['get', 'has'])
            ->getMockForAbstractClass();
        $container->expects($this->any())->method('has')->with($this->equalTo('request'))->will($this->returnValue(false));
        
        $resolver = new DependencyResolver($container);
        
        $method = new ReflectionMethod(DependencyResolverTestClassNoConstructor::class, 'inputTest');
        $parameter = $method->getParameters()[0];
        $value = $resolver->resolve($parameter);
        $this->assertEquals('cat', $value);
    }
    
    public function testExecute()
    {
        $pimple = new Pimple\Container();
        $pimple['response'] = function () {
            return $this->getMockForAbstractClass(ResponseInterface::class);
        };
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
    
        $resolver = new DependencyResolver($container);
        $method = new ReflectionMethod(DependencyResolverTestClassNoConstructor::class, 'inputTestTwo');
        $result = $resolver->execute($method, new DependencyResolverTestClassNoConstructor());
    
        $this->assertInstanceOf(ResponseInterface::class, $result[0]);
        $this->assertInstanceOf(ServerRequestInterface::class, $result[1]);
    }
    
    /**
     * @expectedException \ReflectionException
     * @expectedExceptionMessageRegExp /Method (.*?) does not exist/
     */
    public function testExecuteMethodNotExists()
    {
        $pimple = new Pimple\Container();
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
        
        $resolver = new DependencyResolver($container);
        $resolver->execute(new ReflectionMethod(DependencyResolverTestClass::class, 'fake'), new DependencyResolverTestClass($container->get('request')));
    }
    
    /**
     * @expectedException  \ReflectionException
     * @expectedExceptionMessageRegExp /Given object is not an instance of the class this method was declared in/
     */
    public function testExecuteMethodNotFromObject()
    {
        $pimple = new Pimple\Container();
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
    
        $resolver = new DependencyResolver($container);
        $resolver->execute(new ReflectionMethod(DependencyResolverTestClass::class, 'inputTest'), new DependencyResolverTestClassNoConstructor());
    }
    
    public function testCreateInstanceWithoutConstructor()
    {
        $pimple = new Pimple\Container();
        $pimple['response'] = function () {
            return $this->getMockForAbstractClass(ResponseInterface::class);
        };
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
        
        $resolver = new DependencyResolver($container);
        $object = $resolver->createInstance(new ReflectionClass(DependencyResolverTestClassNoConstructor::class));
        
        $this->assertInstanceOf(DependencyResolverTestClassNoConstructor::class, $object);
    }
    
    public function testCreateInstanceWithConstructor()
    {
        $pimple = new Pimple\Container();
        $pimple['response'] = function () {
            return $this->getMockForAbstractClass(ResponseInterface::class);
        };
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
    
        $resolver = new DependencyResolver($container);
        $object = $resolver->createInstance(new ReflectionClass(DependencyResolverTestClass::class));
    
        $this->assertInstanceOf(DependencyResolverTestClass::class, $object);
        $this->assertAttributeInstanceOf(ServerRequestInterface::class, 'request', $object);
    }
    
    public function testCall()
    {
        $pimple = new Pimple\Container();
        $pimple['response'] = function () {
            return $this->getMockForAbstractClass(ResponseInterface::class);
        };
        $pimple['request'] = function () {
            return $this->getMockForAbstractClass(ServerRequestInterface::class);
        };
        $container = new \Pimple\Psr11\Container($pimple);
        
        $resolver = new DependencyResolver($container);
        $result = $resolver->call(new ReflectionFunction('DependencyResolverFunctionCall'));
        
        $this->assertInstanceOf(ResponseInterface::class, $result);
    }
}

class DependencyResolverTestClass {
    public $request;
    
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }
    
    public function inputTest(ResponseInterface $response, ServerRequestInterface $request)
    {
        return [$response, $request];
    }
}

class DependencyResolverTestClassNoConstructor {
    public function inputTest($request = 'cat'){
        return $request;
    }
    
    public function inputTestTwo(ResponseInterface $response, ServerRequestInterface $request){
        return [$response, $request];
    }
}

function DependencyResolverFunctionCall(ResponseInterface $response)
{
    return $response;
}