<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
/**
 * @noinspection PhpUnhandledExceptionInspection
 */
/**
 * @noinspection PhpUnusedLocalVariableInspection
 */
declare(strict_types=1);

use Apine\Core\Config\Handlers\JsonFileHandler;
use Apine\Core\Error\ErrorHandler;
use Apine\Core\Json\Json;
use PHPUnit\Framework\TestCase;

class JsonFileHandlerTest extends TestCase
{
    static private $filename = './testConfig';
    
    static private $jsonString = '{
        "name" : "value",
        "array" : [
            1,
            2,
            3
        ],
        "object" : {
            "name" : "object",
            "array" : [
                {
                    "number" : "one",
                    "value" : 1
                },
                {
                    "number" : "two",
                    "value" : 2
                },
                {
                    "number" : "three",
                    "value" : 3
                }
            ]
        }
    }';
    
    public function setUp()
    {
        ErrorHandler::set(E_ALL); // Set up error handling so that notice in file manipulation convert to Exception
    }
    
    public function tearDown()
    {
        ErrorHandler::unset();
    }
    
    /**
     * @beforeClass
     */
    public static function createTestFile(): void
    {
        $resource = fopen(self::$filename, 'wb');
        fwrite($resource, self::$jsonString);
        fclose($resource);
    }
    
    /**
     * @afterClass
     */
    public static function deleteTestFile(): void
    {
        unlink(self::$filename);
        unlink(self::$filename.'bad');
    }
    
    public function testConstructor(): void
    {
        $handler = new JsonFileHandler(self::$filename);
        $this->assertAttributeNotEmpty('data', $handler);
        $this->assertAttributeInstanceOf(Json::class, 'data', $handler);
    }
    
    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessageRegExp /File (.*?) not found/
     */
    public function testConstructorFileNotFound(): void
    {
        $handler = new JsonFileHandler(self::$filename . 'hjsdfkhjsfd');
    }
    
    /**
     * @expectedException \Apine\Core\Json\JsonInvalidFormatException
     * @expectedExceptionMessage Invalid JSON string
     */
    public function testConstructorInvalidFormat(): void
    {
        $bad_content = 'invalid format';
        $resource = fopen(self::$filename . 'bad', 'a+b');
        fwrite($resource, $bad_content);
        fclose($resource);
    
        $handler = new JsonFileHandler(self::$filename . 'bad');
    }
    
    public function testFind(): void
    {
        // Test with a value we know should be there
        $handler = new JsonFileHandler(self::$filename);
        $this->assertTrue($handler->find('name'));
        $this->assertFalse($handler->find('jlksdfjklsdf'));
    }
    
    public function testFetch(): void
    {
        // Test with a value we know should be there
        $handler = new JsonFileHandler(self::$filename);
        $this->assertEquals('value', $handler->fetch('name'));
    }
    
    public function testFetchValueNotSet(): void
    {
        $handler = new JsonFileHandler(self::$filename);
        $this->assertNull($handler->fetch('hdflkhjda'));
    }
    
    public function testDefine(): void
    {
        $handler = new JsonFileHandler(self::$filename);
        $oldValue = $handler->fetch('name');
        $handler->define('name', 'new value');
        $this->assertNotEquals($oldValue, $handler->fetch('name'));
        $this->assertEquals('new value', $handler->fetch('name'));
    }
    
    public function testCommit(): void
    {
        $handler = new JsonFileHandler(self::$filename);
        $handler->define('name', 'new');
        $handler->commit();
        
        $newContents = file_get_contents(self::$filename);
        $this->assertNotEquals($newContents, self::$jsonString);
        
        self::createTestFile();
    }
    
    public function testFlush(): void
    {
        $handler = new JsonFileHandler(self::$filename);
        $handler->define('name', 'new');
        
        $handler->flush();
        
        $this->assertNotEquals('new', $handler->fetch('name'));
    }
}
