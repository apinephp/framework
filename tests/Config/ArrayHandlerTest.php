<?php
/**
 * APIne Framework
 *
 * @link      https://gitlab.com/apinephp/framework
 * @copyright Copyright (c) 2018 Tommy Teasdale
 * @license   https://gitlab.com/apinephp/framework/blob/master/LICENSE (MIT License)
 */
/**
 * @noinspection PhpUnhandledExceptionInspection
 */
/**
 * @noinspection PhpUnusedLocalVariableInspection
 */
declare(strict_types=1);

use Apine\Core\Config\Handlers\ArrayHandler;
use Apine\Core\Error\ErrorHandler;
use Apine\Core\Json\Json;
use PHPUnit\Framework\TestCase;

class ArrayHandlerTest extends TestCase
{
    static private $array = [
        'name' => 'value',
        'array' => [
            1, 2, 3
        ],
        'object' => array(
            'name' => 'object',
            'array' => [
                [
                    'number' => 'one',
                    'value' => 1
                ],
                [
                    'number' => 'two',
                    'value' => 2
                ],
                [
                    'number' => 'three',
                    'value' => 3
                ]
            ]
        )
    ];
    
    public function setUp()
    {
        ErrorHandler::set(E_ALL); // Set up error handling so that notice in file manipulation convert to Exception
    }
    
    public function tearDown()
    {
        ErrorHandler::unset();
    }
    
    public function testConstructor(): void
    {
        $handler = new ArrayHandler(self::$array);
        $this->assertAttributeNotEmpty('data', $handler);
        $this->assertAttributeInternalType('array', 'data', $handler);
    }
    
    /**
     * @expectedException \TypeError
     */
    public function testConstructorParameterNotArray(): void
    {
        $handler = new ArrayHandler('hjsdfkhjsfd');
    }
    
    public function testFind(): void
    {
        // Test with a value we know should be there
        $handler = new ArrayHandler(self::$array);
        $this->assertTrue($handler->find('name'));
        $this->assertFalse($handler->find('jlksdfjklsdf'));
    }
    
    public function testFetch(): void
    {
        // Test with a value we know should be there
        $handler = new ArrayHandler(self::$array);
        $this->assertEquals('value', $handler->fetch('name'));
    }
    
    public function testFetchValueNotSet(): void
    {
        $handler = new ArrayHandler(self::$array);
        $this->assertNull($handler->fetch('hdflkhjda'));
    }
    
    public function testDefine(): void
    {
        $handler = new ArrayHandler(self::$array);
        $oldValue = $handler->fetch('name');
        $handler->define('name', 'new value');
        $this->assertNotEquals($oldValue, $handler->fetch('name'));
        $this->assertEquals('new value', $handler->fetch('name'));
    }
    
    public function testCommit(): void
    {
        $handler = new ArrayHandler(self::$array);
        $handler->define('name', 'new');
        
        $handler->commit();
        
        $this->assertEquals(
            $this->getObjectAttribute($handler, 'data'),
            $this->getObjectAttribute($handler, 'defaultState')
        );
    }
    
    public function testFlush(): void
    {
        $handler = new ArrayHandler(self::$array);
        $handler->define('name', 'new');
        
        $handler->flush();
        
        $this->assertNotEquals('new', $handler->fetch('name'));
    }
}
